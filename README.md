# TranscriptThing

TranscriptThing is a desktop transcription tool that allows for offline
subtitling of any application that currently outputs audio.
This thing transcribes audio from monitor device (or mic), sending it to an
on-screen-display (OSD) via DBus.
Please note that this is a highly experimental thing, mainly to see if such
thing is actually feasible.

![](transcriptthing/resources/icon.png)

## Goals

Offline text-to-speech (TTS) is getting more and more attention over the years, as
models are getting better – mainly thanks to improved neural network architectures
and more advanced training corpora.

However, good TTS is either integrated into online solutions (e.g. commercial
video platforms) or exists in the form of acedemic code without any interfaces
to the desktop. For the sake of usability, consistency and privacy, a single,
user-friendly offline tool is desirable.

With TranscriptThing, I want to bring state-of-the-art TTS to the Linux desktop.
The general idea is to show subtitles on the desktop, which are:

* **Offline-only.** Not directing any of your speech / transcript through the cloud.
* **Service-agnostic.** It should work with any video or audio player by attaching itself to the sound system.
* **Easy to install.** Ideally via package manager, batteries included.
* **Lean, but easy to extend.** This implies a decent and comprehensible code base and high configurability.


## Planned Features

Features that are planned to accomplish the goals in previous section:

* Easy installation routine (yes, I consider this a feature!)
* Extensions like punctuation, translation and speaker recognition

See "issues" for more details.

## Implemented Features

* Desktop transcription through offline VOSK models
* Downloading transcription & translation models automatically
* Translation of transcripted text
* Dumping transcript to command line (you can dump it to a file)
* Rudimentary on-screen-display (OSD) for desktop subtitles

## Getting Started

### Prerequisites

Please be aware of the fact that VOSK models (the good ones) take quite some
space on hard drive.
Make sure you have at least 7 GB free, because that's the space that
two models (English + one alternative language) typically require.
Install python3 and virtualenv using your system's package manager.
Make sure PulseAudio is up and running.
Finally, create a virtualenv:

```bash
virtualenv env
source env/bin/activate
pip install -r requirements.txt
```


### Initialization

To initalize, run:

```bash
python3 ./run.py init
```

this will download an English VOSK model and one model for your system language
(in case your system language is not English), if there is a model available for it.



### Running

You'll need to start two processes:

```bash
python3 ./run.py trans
```

to start the transcription thing (going to be a daemon in the future) and

```bash
python3 ./run.py osd
```

for the OSD.
A tray icon should show up which allows you to quit Transcript Thing.

#!/usr/bin/env python3
import os
import subprocess

from setuptools.command.install import install
from setuptools import setup


class InstallSystemd(install):
  def run(self):
    install.run(self)
    current_dir_path = os.path.dirname(os.path.realpath(__file__))
    create_service_script_path = os.path.join(
        current_dir_path,
        'scripts',
        'install_systemd.sh'
    )
    subprocess.check_output([create_service_script_path])


setup(
    name='transcriptthing',
    version='1.0',
    description='Desktop transcription tool',
    author='eichkat3r',
    url='https://codeberg.org/eichkat3r/transcript-thing',
    install_requires=[
        'click',
        'pulsectl',
        'pystray',
        'screeninfo',
        'sounddevice',
        'vosk',
        'wget'
    ],
    include_package_data=True,
    package_data={
        'transcriptthing': [
            'resources/*'
        ]
    },
    data_files=[
        ('share/applications', ['transcriptthing.desktop']),
        ('share/icons', ['transcriptthing.png'])
    ],
    packages=['transcriptthing', 'transcriptthing.backend'],
    entry_points='''
        [console_scripts]
        transcriptthing = transcriptthing:cli
    ''',
    cmdclass={ 'install': InstallSystemd }
)

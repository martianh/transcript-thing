#!/bin/bash

old_cwd=$(pwd)
script_dir="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
root_dir="$script_dir/.."
cd $root_dir

cp etc/systemd/system/* /etc/systemd/system/

systemctl daemon-reload

cd $old_pwd

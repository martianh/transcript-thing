# Hacking


## Creating a new OSD

TranscriptThing uses DBus to communicate between daemon and OSD.
To get an idea of how this communication works, I recommend taking a look at
osd.py, which implements a rudimentary OSD in tkinter.
You can also take a look at the DBus interface using qdbusviewer or similar
UI applications.
You will find that there are currently two methods implemented:
GetResult and GetPartial.
GetResult will provide you the full sentence once it has been transcribed,
while GetPartial will provide you preliminary partial results.
Currently, those are implemented as synchronous methods which need to be
polled.
Asynchronous communication using signals is future work.

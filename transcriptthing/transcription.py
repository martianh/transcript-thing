import fcntl
import json
import logging
import os
import subprocess

import pulsectl
import sounddevice as sd

import dbus
import dbus.service
import dbus.mainloop.glib
from gi.repository import GObject, GLib

from .translation import ArgosTranslator
from .utils import get_language_code, get_backend


def get_monitors(include_mics=False):
    # TODO find a platform independent way to get monitor device
    # TODO find a platform independent way to create a new loopback device
    #      for standard audio output
    with pulsectl.Pulse('transcriptthing') as pulse:
        sources = pulse.source_list()
        monitors = [
            s.name for s in sources
            if s.description.startswith('Monitor') or include_mics
        ]
        return monitors


def start_recording(device, samplerate):
    # TODO find a platform independent way to record from device
    # pyaudio might be an option?
    cmd = (
        'parec',
        '--record',
        #f'--rate={samplerate}',
        '--channels=1',
        f'--device={device}',
        '--format=s16ne',
        '--latency=10',
    )
    ps = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    stdout = ps.stdout
    # make file handle non-blocking
    flags = fcntl.fcntl(stdout.fileno(), fcntl.F_GETFL)
    fcntl.fcntl(stdout, fcntl.F_SETFL, flags | os.O_NONBLOCK)
    return stdout


class ChannelObject(dbus.service.Object):
    result: str = ''
    partial: str = ''

    def __init__(self, bus, path):
        dbus.service.Object.__init__(self, bus, path)
        self.monitor = get_monitors()[0]

    @dbus.service.method(
        'de.eichkat3r.TranscriptThing.ChannelInterface',
        in_signature='', out_signature='as'
    )
    def GetMonitors(self):
        return get_monitors()

    @dbus.service.method(
        'de.eichkat3r.TranscriptThing.ChannelInterface',
        in_signature='s', out_signature='b'
    )
    def SetMonitor(self, monitor):
        if monitor not in get_monitors():
            return False
        self.monitor = monitor
        return True

    @dbus.service.method(
        'de.eichkat3r.TranscriptThing.ChannelInterface',
        in_signature='', out_signature='s'
    )
    def GetMonitor(self):
        return self.monitor

    @dbus.service.method(
        'de.eichkat3r.TranscriptThing.ChannelInterface',
        in_signature='', out_signature='s'
    )
    def GetResult(self):
        return self.result

    @dbus.service.method(
        'de.eichkat3r.TranscriptThing.ChannelInterface',
        in_signature='', out_signature='s'
    )
    def GetPartial(self):
        return self.partial


def transcribe():
    # establishing DBus connection
    print('Connecting to DBus')
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    bus = dbus.SessionBus()
    name = dbus.service.BusName('de.eichkat3r.TranscriptThing', bus)
    obj = ChannelObject(bus, '/ChannelObject')

    # TODO get appropriate sample rate for device
    samplerate = 44100
    # TODO find out if this is a reasonable block size
    block_size = 100000

    language = get_language_code()
    translator = ArgosTranslator(language)
    translator.select_output_language('de')
    translator.select_model()

    # select backend from config and initialize
    backend = get_backend()
    backend.start(language, samplerate)
    
    monitor = ''
    fd = None
    
    def callback():
        nonlocal monitor
        nonlocal fd

        if monitor != obj.monitor:
            monitor = obj.monitor
            fd = start_recording(obj.monitor, samplerate)
        if fd is None:
            return
        data = fd.read(block_size)
        if data:
            backend.process(data)
            # TODO returning partial results seems to be a VOSK specific thing
            #      how to deal with that?
            obj.partial = translator.translate(backend.partial)
            obj.result = translator.translate(backend.result)
        return True

    logging.info('Setting up GLib main loop')
    GObject.timeout_add_seconds(0.1, callback)
    mainloop = GLib.MainLoop()
    logging.info('Transcript Thing ready to use')
    mainloop.run()
    # TODO add this to signal handler somehow so it gets called on Ctrl+C
    # (not too much of a problem since typically no cleanup is required)
    backend.end()

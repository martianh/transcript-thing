from .voskbackend import VOSKBackend


BACKENDS = {
    'vosk': VOSKBackend
}

try:
    import deepspeech
    from .deepspeechbackend import DeepspeechBackend
    BACKENDS['deepspeech'] = DeepspeechBackend
except ImportError:
    pass


class TranscriptionBackend:
    def __init__(self):
        self.partial = ''
        self.result = ''

    def download_model(self, language):
        """
        Download model files for a specified language code.
        """
        pass

    def get_models_path(self):
        """
        Get path where models for this backend are stored.
        """
        pass

    def get_language_model_path(self, language):
        """
        Get path where a specific language model is stored.
        """
        pass

    def get_model_url(self, language, default=None):
        """
        Get download URL for zipped language model.
        """
        pass

    def start(self, language, samplerate=44100):
        """
        Start transcription in a specified language.
        """
        pass

    def process(self, buffer):
        """
        Process binary waveform buffer.
        """
        pass

    def end(self):
        """
        Cleanup after transcription if necessary.
        """
        pass

import json
import os
import shutil

import click
import urllib.request
import vosk
import wget
# TODO use shutil for zip extraction instead
import zipfile

from .backend import TranscriptionBackend
from ..checksum import validate_checksum
from ..paths import MODELS_PATH


class VOSKBackend(TranscriptionBackend):
    def __init__(self):
        super().__init__()
        self.model_urls = {
            'de': 'https://alphacephei.com/vosk/models/vosk-model-de-0.21.zip',
            'en': 'https://alphacephei.com/vosk/models/vosk-model-en-us-0.22.zip',
            'fr': 'https://alphacephei.com/vosk/models/vosk-model-fr-0.22.zip',
            'es': 'https://alphacephei.com/vosk/models/vosk-model-small-es-0.22.zip',
            'pt': 'https://alphacephei.com/vosk/models/vosk-model-small-pt-0.3.zip',
            'ru': 'https://alphacephei.com/vosk/models/vosk-model-ru-0.22.zip'
        }
        # TODO add more checksums
        self.model_checksums = {
            'de': '245060756f8d8394fc5b13639cf220b7620205795f30f37b6823878d4f603b2a',
            'en': '47f9a81ebb039dbb0bd319175c36ac393c0893b796c2b6303e64cf58c27b69f6',
            'fr': None,
            'es': None,
            'pt': None,
            'ru': None
        }
        self.model_dirs = {
            # TODO there are more elegant ways to do this
            key: self.model_urls[key].split('/')[-1][:-len('.zip')] for key in self.model_urls
        }

    def get_models_path(self):
        return os.path.join(MODELS_PATH, 'vosk')

    def get_language_model_path(self, language):
        return os.path.join(self.get_models_path(), self.model_dirs[language])

    def get_model_url(self, language, default=None):
        return self.model_urls.get(language, default)

    def download_model(self, language):
        model_url = self.get_model_url(language, None)
        if not model_url:
            click.secho(f'No model available.', fg='yellow')
            return
        os.makedirs(self.get_models_path(), exist_ok=True)
        # check if enough disk space exists for zip
        stat = shutil.disk_usage(self.get_models_path())
        d = urllib.request.urlopen(model_url)
        info = d.info()
        remote_size = int(info['Content-Length'])
        if stat.free < remote_size:
            click.secho(f'Not enough disk space to store model zip.', fg='red')
            click.secho(f'File: {remote_size}, Free: {stat.free}')
            return
        zip_path = os.path.join(self.get_models_path(), 'model.zip')
        wget.download(model_url, out=zip_path)
        print()

        # perform checksum validation
        checksum = self.model_checksums.get(language, None)
        if validate_checksum(checksum, zip_path):
            click.secho(f'Extracting zip...', fg='blue')
            with zipfile.ZipFile(zip_path, 'r') as z:
                z.extractall(self.get_models_path())
            # FIXME react if user pressed Ctrl+C by deleting temporary files
        os.remove(zip_path)

    def start(self, language, samplerate=44100):
        modelpath = self.get_language_model_path(language)
        model = vosk.Model(modelpath)
        self.rec = vosk.KaldiRecognizer(model, samplerate)

    def process(self, buffer):
        if self.rec.AcceptWaveform(buffer):
            result = self.rec.Result()
            self.result = json.loads(result)['text']
        else:
            partial = self.rec.PartialResult()
            self.partial = json.loads(partial)['partial']

    def end(self):
        pass

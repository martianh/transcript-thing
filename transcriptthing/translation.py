import os

from argostranslate import package, translate
import click
import hashlib
import wget

from .checksum import validate_checksum
from .paths import MODELS_PATH


class ArgosTranslator:
    def __init__(self, input_language):
        self.model = None
        self.input_language = input_language
        self.output_language = input_language
        self.model_urls = {
            ('de', 'en'): 'https://argosopentech.nyc3.digitaloceanspaces.com/argospm/translate-de_en-1_0.argosmodel',
            ('en', 'de'): 'https://argosopentech.nyc3.digitaloceanspaces.com/argospm/translate-en_de-1_0.argosmodel'
        }
        self.model_checksums = {
            ('de', 'en'): 'bc8b6dab53deda59a1d0c314f43127f20679b009aefef290bd6e9229e14fcc39',
            ('en', 'de'): '8d9cd82aedd55a37c3ad98668c7e6d6d806e353435a6d288ab541aa660d11473'
        }
        self.model_filenames = {
            k: v.split('/')[-1] for k, v in self.model_urls.items()  
        }

    def get_models_path(self):
        return os.path.join(MODELS_PATH, 'argos')

    def install_language(self, input_language, output_language):
        pair = (input_language, output_language)
        # TODO react if pair not available
        filename = self.model_filenames.get(pair)
        path = os.path.join(self.get_models_path(), filename)
        checksum = self.model_checksums.get(pair)
        if os.path.isfile(path):
            if validate_checksum(checksum, path) and checksum is not None:
                # no need to download model, since file exists and checksum matches
                return
        os.makedirs(get_models_path(), exist_ok=True)
        # TODO check if enough disk space exists
        url = self.model_urls.get(pair)
        wget.download(url, out=path)
        print()
        package.install_from_path(path)
        if not validate_checksum(checksum, path):
            os.remove(path)

    def select_input_language(self, input_language):
        self.input_language = input_language

    def select_output_language(self, output_language):
        self.output_language = output_language

    def select_model(self):
        if self.input_language == self.output_language:
            self.model = None
            return
        pair = (self.input_language, self.output_language)
        if pair not in self.model_urls:
            click.secho('Translation model {pair[0]} -> {pair[1]} not available.', fg='red')
            self.model = None
            return

        # install if not available
        self.install_language(self.input_language, self.output_language)
        installed_languages = translate.get_installed_languages()
        languages = { lang.code: lang for lang in installed_languages }
        input_model = languages[self.input_language]
        output_model = languages[self.output_language]
        self.model = input_model.get_translation(output_model)

    def translate(self, text):
        if self.model is None:
            return text
        result = self.model.translate(text)
        return result

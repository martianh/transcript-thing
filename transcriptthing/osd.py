import os
import sys
from random import seed, choice
from string import ascii_letters
from threading import Thread

import dbus

import screeninfo
import tkinter as tk
import pystray
from PIL import Image

from .linebuffer import LineBuffer


def get_primary_width_and_x():
    """
    Return width and x coordinate of primary screen.
    """
    for m in screeninfo.get_monitors():
        if m.is_primary:
            return m.width, m.x


def show_osd():
    session_bus = dbus.SessionBus()
    bus_object = session_bus.get_object(
        'de.eichkat3r.TranscriptThing',
        '/ChannelObject'
    )
    channel_iface = dbus.Interface(
        bus_object,
        dbus_interface='de.eichkat3r.TranscriptThing.ChannelInterface'
    )

    active = True
    linebuffer = LineBuffer()

    def refresh():
        nonlocal linebuffer
        nonlocal active

        if not active:
            return
        partial = channel_iface.GetPartial()
        result = channel_iface.GetResult()
        linebuffer.set_result(result)
        linebuffer.set_partial(partial)
        
        if linebuffer.changed:
            lines = linebuffer.get_lines()

            root.deiconify()

            for i, label in enumerate(labels):
                j = linebuffer.num_lines - i - 1
                label.config(text=lines[j], justify=tk.LEFT)
        root.after(10, refresh)

    labels = []
    root = tk.Tk()
    root.wm_overrideredirect(True)
    root.wm_attributes('-type', 'splash')
    root.attributes('-alpha', 0.9)

    def start_move(event):
        nonlocal root
        root.x = event.x
        root.y = event.y

    def stop_move(event):
        nonlocal root
        root.x = None
        root.y = None

    def on_motion(event):
        dx = event.x - root.x
        dy = event.y - root.y
        x = root.winfo_x() + dx
        y = root.winfo_y() + dy
        root.geometry(f'+{x}+{y}')

    num_lines = 3
    line_height = 32
    banner_height = line_height * num_lines
    padding = 16
    margin = 16

    width, x = get_primary_width_and_x()
    banner_width = max(int(width * 0.5), 720)
    banner_x = x + (width - banner_width) // 2
    banner_y = int(root.winfo_screenheight() - banner_height - margin)

    root.geometry(f'{banner_width}x{banner_height}+{banner_x}+{banner_y}')

    # hide OSD if it was double-clicked
    root.bind('<Double-Button-1>', lambda evt: root.withdraw())
    root.bind('<ButtonPress-1>', start_move)
    root.bind('<ButtonRelease-1>', stop_move)
    root.bind('<B1-Motion>', on_motion)

    for i, line in enumerate(range(num_lines)):
        gv = (num_lines - i) * 2
        fg = f'#{gv}{gv}{gv}' if i != num_lines - 1 else 'black'
        size = 12 - (num_lines - i - 1)
        label = tk.Label(
            text='', font=('monospace', size), fg=fg,
            justify=tk.LEFT, width=80, anchor='w'
        )
        labels.append(label)
        labels[len(labels) - 1].pack(expand=True)

    print('Waiting for transcription service to start...')
    refresh()
    root.wait_visibility(root)

    def quit_window():
        root.destroy()


    icon_path = os.path.join(os.path.dirname(__file__), 'resources', 'icon.png')
    image = Image.open(icon_path)

    # add monitors to systray menu
    monitors = channel_iface.GetMonitors()

    def select_monitor(m):
        def inner():
            channel_iface.SetMonitor(m)
        return inner

    def is_selected(item):
        return channel_iface.GetMonitor() == item.text

    submenu = [
        pystray.MenuItem(
            monitor, select_monitor(monitor),
            checked=is_selected, radio=True
        )
        for monitor in monitors
    ]

    def toggle_active():
        nonlocal active
        active = not active
        if not active:
            root.withdraw()
        else:
            root.deiconify()

    menu = [
        pystray.MenuItem('Active', toggle_active, checked=lambda _: active),
        pystray.MenuItem('Monitor', pystray.Menu(*submenu)),
        pystray.MenuItem('Quit', quit_window)
    ]
    icon = pystray.Icon('name', image, 'TranscriptThing', menu)

    # thread for systray icon
    thread = Thread(target=icon.run, daemon=True)
    thread.start()

    root.mainloop()

import click
import hashlib


def validate_checksum(checksum, filename):
    if checksum is None:
        click.secho(f'No checksum available for {language}. Use at your own risk.', fg='yellow')
        return True
    else:
        click.secho(f'Validating checksum...', fg='blue')
        sha256 = hashlib.sha256()
        with open(filename, 'rb') as f:
            while True:
                data = f.read(65536)
                if not data:
                    break
                sha256.update(data)
        file_hash = sha256.hexdigest()
        if checksum != file_hash:
            click.secho(f'Invalid checksum. This might be a bug, a server error or transmission problem.', fg='red')
            click.secho(f'expected: {checksum}')
            click.secho(f'download: {file_hash}')
            return False
    return True

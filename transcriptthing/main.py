import click

from .filedump import filedump
from .transcription import transcribe
from .textdump import textdump
from .initialize import initialize
from .osd import show_osd


@click.group()
def cli():
    pass


@cli.command(help='show on-screen-display')
def osd():
    show_osd()


@cli.command(help='initialize speech2text model')
def init():
    initialize()


@cli.command(help='transcribe audio to dbus')
def trans():
    transcribe()


@cli.command(help='transcribe audio to stdout')
def dump():
    textdump()


@cli.command(help='transcribe audio file to stdout')
@click.argument('filename', type=click.Path(exists=True))
@click.pass_context
def dumpfile(ctx, filename):
    filedump(filename)

class LineBuffer:
    """
    """

    def __init__(self, num_lines=3, max_chars=80):
        self.num_lines = num_lines
        self.max_chars = max_chars
        self.use_partial = True
        self.result = ''
        self.partial = ''
        self.changed = False

    def set_result(self, result):
        if result != self.result:
            self.changed = True
            self.use_partial = False
        self.result = result

    def set_partial(self, partial):
        if partial != self.partial:
            self.changed = True
            self.use_partial = True
        self.partial = partial

    def get_lines(self):
        if self.use_partial:
            tokens = self.partial.split(' ')
        else:
            tokens = self.result.split(' ')
        lines = []
        line = ''
        for i, token in enumerate(tokens):
            addendum = (line + ' ' + token).strip()
            if len(addendum) > self.max_chars:
                lines.append(line)
                line = token
            else:
                line = addendum
        lines.append(line)
        if len(lines) > self.num_lines:
            lines = lines[len(lines) - self.num_lines:]
        lines = lines[::-1]
        while len(lines) < self.num_lines:
            lines.append('')
        self.changed = False
        return lines
